from flask import Flask
from datetime import datetime, timedelta
from influxdb_client import InfluxDBClient
from sklearn.svm import SVR
from sklearn.preprocessing import StandardScaler

import pandas as pd

import os

token = os.environ['DOCKER_INFLUXDB_TOKEN']
url = "http://62.84.115.148:8086"
org = "traderbot"
bucket = "traderbot"
measurement = "candles"
fields = [("open", "first"), ("close", "last"), ("high", "max"), ("low", "min")]
# , ("value", "mean"), ("volume", "sum")


def get_fields_via_function(ticket, start, stop, period_in_minutes, field, function):
    with InfluxDBClient(url=url, token=token, org=org) as client:
        query = f"from(bucket: \"{bucket}\") |> range(start: {start}, stop: {stop}) |> filter(fn: (r) => r._measurement == \"{measurement}\" and r.ticket == \"{ticket}\" and r._field == \"{field}\") |> window(every: {period_in_minutes}m) |> {function}()"
        tables = client.query_api().query(query, org=org)
        for table in tables:
            for record in table.records:
                yield record["_start"], record["_value"]


def get_candles(ticket, start, stop, period_in_minutes):
    ans = dict()
    for field in fields:
        for field_value in get_fields_via_function(ticket, start, stop, period_in_minutes, field[0], field[1]):
            if field[0] not in ans:
                ans[field[0]] = list()
            ans[field[0]].append(field_value)
    return ans


def get_data(ticket, start, stop, period_in_minutes) -> pd.DataFrame:
    candles = get_candles(ticket, start, stop, period_in_minutes)
    table = dict()
    index = []
    f = 0
    for i in candles.keys():
        table[i] = []
        for j in candles[i]:
            if f == 0:
                index.append(j[0])
            table[i].append(j[1])
        f = 1
    data = pd.DataFrame.from_dict(table, orient='index', columns=index)
    return data.transpose()


app = Flask(__name__)


@app.route('/predict/<string:ticket>/<int:period_in_minutes>', methods=['GET'])
def predict(ticket, period_in_minutes) -> str:
    n_param = 5
    now = datetime.now() - timedelta(minutes=period_in_minutes)
    past = now - timedelta(minutes=period_in_minutes * 1000)
    try:
        data = get_data(ticket, past.strftime("%Y-%m-%dT%H:%M:%SZ"), now.strftime("%Y-%m-%dT%H:%M:%SZ"), period_in_minutes)
    except:
        return '0'
    # data = get_data("gazp", "2021-10-15T00:00:00Z", "2021-10-16T00:00:00Z", 10)
    X = data.iloc[:, []]
    y = data.iloc[:, 1]
    X = X[n_param:]

    for i in range(n_param):
        X[f'{i + 1} C'] = y.shift(i + 1)
        X[f'{i + 1} H'] = data.iloc[:, 2].shift(i + 1)
        X[f'{i + 1} L'] = data.iloc[:, 3].shift(i + 1)
        X[f'{i + 1} O'] = data.iloc[:, 0].shift(i + 1)
    y = y[n_param:]

    scaler = StandardScaler()
    svr = SVR(C=1000, kernel='rbf', gamma='auto')

    n = y.size
    X_train = X.iloc[:n]
    y_train = y[:n]

    X_test = []
    for i in range(n_param):
        X_test.append(y[-i - 1])
        X_test.append(data.iloc[-i - 1, 2])
        X_test.append(data.iloc[-i - 1, 3])
        X_test.append(data.iloc[-i - 1, 0])

    X_test = [X_test]

    X_train_scaled = scaler.fit_transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    svr.fit(X_train_scaled, y_train)

    prediction = svr.predict(X_test_scaled)

    return str(prediction[0])


if __name__ == '__main__':
    app.run(host="0.0.0.0")
