using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot
{
    public class DailyUpdateCandles : IJob
    {
        private readonly EFGenericRepository<Ticket> _tickets;
        private readonly UnionActionPriceService _unionActionPriceService;

        public DailyUpdateCandles(UnionActionPriceService unionActionPriceService, EFGenericRepository<Ticket> tickets)
        {
            _unionActionPriceService = unionActionPriceService;
            _tickets = tickets;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var tasks = new List<Task>();
            foreach (var ticket in _tickets.Get().OrderBy(ticket => ticket.Id))
                await _unionActionPriceService.UpdateTicketCandles(ticket);

            await Task.WhenAll(tasks);
        }
    }
}