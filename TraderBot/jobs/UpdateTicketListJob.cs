using System.Linq;
using System.Threading.Tasks;
using Quartz;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot
{
    public class UpdateTicketListJob : IJob
    {
        private readonly EFGenericRepository<Ticket> _tickets;
        private readonly UnionActionPriceService _unionActionPriceService;

        public UpdateTicketListJob(UnionActionPriceService unionActionPriceService, EFGenericRepository<Ticket> tickets)
        {
            _unionActionPriceService = unionActionPriceService;
            _tickets = tickets;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var existedTickets = _tickets.Get().Select(ticket => ticket.Name).ToHashSet();
            var currentTicketsData = await _unionActionPriceService.GetTicketsList();
            foreach (var (source, tickets) in currentTicketsData)
            foreach (var ticketName in tickets)
                if (!existedTickets.Contains(ticketName))
                {
                    var ticket = new Ticket
                    {
                        Name = ticketName,
                        Source = source
                    };
                    existedTickets.Add(ticketName);
                    _tickets.Create(ticket);
                    _tickets.Save();
                }
        }
    }
}