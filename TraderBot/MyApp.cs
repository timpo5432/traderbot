using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Telegram.Bot;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot
{
    public class MyApp
    {
        private readonly IJobFactory _factory;
        private readonly UnionActionPriceService _unionActionPriceService;
        private readonly ITelegramBotClient bot;
        private readonly IConfigurationRoot config;
        private readonly IActionsDataRepository db;
        private readonly Handlers handlers;
        private readonly IGenericRepository<State> stateRepository;
        private readonly IGenericRepository<User> userRepository;


        public MyApp(IConfigurationRoot config, EFGenericRepository<User> userRepository, IActionsDataRepository db,
            EFGenericRepository<State> stateRepository, Handlers handlers,
            UnionActionPriceService unionActionPriceService, IJobFactory factory, ITelegramBotClient bot)
        {
            this.userRepository = userRepository;
            this.config = config;
            this.db = db;
            this.stateRepository = stateRepository;
            this.handlers = handlers;
            _unionActionPriceService = unionActionPriceService;
            _factory = factory;
            this.bot = bot;
        }

        public async Task Start()
        {
            var factory = new StdSchedulerFactory();
            var scheduler = await factory.GetScheduler();
            scheduler.JobFactory = _factory;
            await scheduler.Start();

            await DailyCandlesUpdate(scheduler);
            await TicketListUpdate(scheduler);

            var bot = new TelegramBotClient(config.GetConnectionString("TelegramToken"));
            using var cts = new CancellationTokenSource();
            
            // bot.StartReceiving(handlers.HandleUpdateAsync, handlers.HandleErrorAsync, cancellationToken: cts.Token);
            await bot.ReceiveAsync(handlers.HandleUpdateAsync, handlers.HandleErrorAsync, cancellationToken: cts.Token);
        }

        private async Task DailyCandlesUpdate(IScheduler scheduler)
        {
            var j = JobBuilder.Create<DailyUpdateCandles>()
                .WithIdentity("name", "group")
                .Build();

            var t = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .StartAt(DateTimeOffset.Now)
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(12)
                    .RepeatForever())
                .Build();

            await scheduler.ScheduleJob(j, t);
        }

        private async Task TicketListUpdate(IScheduler scheduler)
        {
            var j = JobBuilder.Create<UpdateTicketListJob>()
                .WithIdentity("name2", "group")
                .Build();

            var t = TriggerBuilder.Create()
                .WithIdentity("trigger2", "group1")
                .StartAt(DateTimeOffset.Now)
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(1)
                    .RepeatForever())
                .Build();

            await scheduler.ScheduleJob(j, t);
        }
    }
}