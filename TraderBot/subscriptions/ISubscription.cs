using Quartz;
using TraderBot.models;

namespace TraderBot.entities
{
    public interface ISubscription
    {
        public string Name { get; }
        public string Description { get; }
        public string ParamsHelpMessage { get; }

        public IJobDetail CreateJob(string ticket, string parametrs, User user);

        public ITrigger CreateTrigger(string ticket, string parametrs, User user);

        public string GetDescription(string data);
        public string GetData(string ticket, string parametrs);
    }
}