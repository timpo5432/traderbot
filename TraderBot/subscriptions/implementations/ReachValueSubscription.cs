using System;
using System.Linq;
using System.Text.Json;
using Quartz;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot.entities
{
    public class ReachValueSubscription : ISubscription
    {
        private readonly UnionActionPriceService _unionActionPriceService;

        public ReachValueSubscription(UnionActionPriceService unionActionPriceService)
        {
            _unionActionPriceService = unionActionPriceService;
        }

        public string Name => "ReachValueSubscription";
        public string Description => "При достижении цены";
        public string ParamsHelpMessage => "Введите цену, которую должна достичь акция \nПример: 228.50";

        public IJobDetail CreateJob(string ticket, string parametrs, User user)
        {
            try {
            var expectedValue = double.Parse(parametrs.Split().First());
            return JobBuilder.Create<ReachValueSubscriptionJob>()
                .WithIdentity("ReachValueSubscription" + user.Subscriptions.Count, "group")
                .UsingJobData("ticket", ticket)
                .UsingJobData("chatId", user.ChatId)
                .UsingJobData("destination",
                    _unionActionPriceService.GetActionPrice(ticket).Result > expectedValue ? "down" : "up")
                .UsingJobData("expectedValue", expectedValue)
                .Build();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ITrigger CreateTrigger(string ticket, string parametrs, User user)
        {
            try {
            return TriggerBuilder.Create()
                .WithIdentity("ReachValueSubscription" + user.Subscriptions.Count, "group")
                .StartAt(DateTimeOffset.Now)
                .WithSimpleSchedule(
                    x => x.WithIntervalInMinutes(1))
                .Build();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string GetDescription(string data)
        {
            var reachValueSubscriptionData = JsonSerializer.Deserialize<ReachValueSubscriptionData>(data);
            return "ожидаемая цена для: " + reachValueSubscriptionData.Ticket + ". Ожидаемая цена: " +
                   reachValueSubscriptionData.ExpectedValue;
        }

        public string GetData(string ticket, string parametrs)
        {
            var expectedValue = double.Parse(parametrs.Split().First());
            var data = new ReachValueSubscriptionData
            {
                Ticket = ticket,
                ExpectedValue = expectedValue
            };
            return JsonSerializer.Serialize(data);
        }
    }
}