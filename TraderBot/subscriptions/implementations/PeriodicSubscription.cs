using System;
using System.Text.Json;
using Quartz;
using TraderBot.models;

namespace TraderBot.entities
{
    public class PeriodicSubscription : ISubscription
    {
        public string Name => "PeriodicSubscription";
        public string Description => "Раз в период";
        public string ParamsHelpMessage => "Введите периодичность уведомления в формате часы:минуты:секунды";

        public IJobDetail CreateJob(string ticket, string parametrs, User user)
        {
            if (parametrs.Split(":").Length != 3) return null;

            try
            {
                return JobBuilder.Create<PeriodicSubscriptionJob>()
                    .WithIdentity("PeriodicSubscription" + user.Subscriptions.Count, "group")
                    .UsingJobData("ticket", ticket)
                    .UsingJobData("chatId", user.ChatId)
                    .Build();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ITrigger CreateTrigger(string ticket, string parametrs, User user)
        {
            try
            {
                var timings = parametrs.Split(":");
                if (timings.Length != 3) return null;

                return TriggerBuilder.Create()
                    .WithIdentity("PeriodicSubscription" + user.Subscriptions.Count, "group")
                    .StartAt(DateTimeOffset.Now)
                    .WithSimpleSchedule(x => BuildSchedule(timings, x))
                    .Build();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string GetDescription(string data)
        {
            var periodicSubscriptionData = JsonSerializer.Deserialize<PeriodicSubscriptionData>(data);
            return "цена для: " + periodicSubscriptionData.Ticket + ". Периодичность:" +
                   periodicSubscriptionData.Hours + ":" + periodicSubscriptionData.Minutes + ":" +
                   periodicSubscriptionData.Seconds;
        }

        public string GetData(string ticket, string parametrs)
        {
            var dataSplitted = parametrs.Split(":");
            var periodicSubscriptionData = new PeriodicSubscriptionData();
            periodicSubscriptionData.Ticket = ticket;
            periodicSubscriptionData.Hours = int.Parse(dataSplitted[0]);
            periodicSubscriptionData.Minutes = int.Parse(dataSplitted[1]);
            periodicSubscriptionData.Seconds = int.Parse(dataSplitted[2]);
            return JsonSerializer.Serialize(periodicSubscriptionData);
        }

        private SimpleScheduleBuilder BuildSchedule(string[] timings, SimpleScheduleBuilder x)
        {
            var seconds = int.Parse(timings[2]);
            var minutes = int.Parse(timings[1]);
            var hours = int.Parse(timings[0]);

            if (seconds != 0) x.WithIntervalInSeconds(seconds);

            if (minutes != 0) x.WithIntervalInMinutes(minutes);

            if (hours != 0) x.WithIntervalInHours(hours);

            x.RepeatForever();
            return x;
        }
    }
}