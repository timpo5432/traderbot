using System;
using System.Linq;
using System.Text.Json;
using Quartz;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot.entities
{
    public class ChangePercentsSubscription : ISubscription
    {
        private readonly UnionActionPriceService _unionActionPriceService;

        public ChangePercentsSubscription(UnionActionPriceService unionActionPriceService)
        {
            _unionActionPriceService = unionActionPriceService;
        }

        public string Name => "ChangePercentsSubscription";
        public string Description => "При изменении на процент";
        public string ParamsHelpMessage => "Введите процент на который должна измениться цена\nПример: -4.5";

        public IJobDetail CreateJob(string ticket, string parametrs, User user)
        {
            try
            {
                var expectedPercents = double.Parse(parametrs.Split().First());
                return JobBuilder.Create<ChangePercentsSubscriptionJob>()
                    .WithIdentity("ChangePercentsSubscription" + user.Subscriptions.Count, "group")
                    .UsingJobData("ticket", ticket)
                    .UsingJobData("chatId", user.ChatId)
                    .UsingJobData("destination", expectedPercents < 0 ? "down" : "up")
                    .UsingJobData("baseValue", _unionActionPriceService.GetActionPrice(ticket).Result)
                    .UsingJobData("expectedPercents", expectedPercents)
                    .Build();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ITrigger CreateTrigger(string ticket, string parametrs, User user)
        {
            try
            {
                return TriggerBuilder.Create()
                    .WithIdentity("ChangePercentsSubscription" + user.Subscriptions.Count, "group")
                    .StartAt(DateTimeOffset.Now)
                    .WithSimpleSchedule(
                        x => x.WithIntervalInMinutes(1))
                    .Build();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string GetDescription(string data)
        {
            var reachValueSubscriptionData = JsonSerializer.Deserialize<ChangePercentsSubscriptionData>(data);
            return "ожидаемое изменение на " + reachValueSubscriptionData.ExpectedPercents + "% для: " +
                   reachValueSubscriptionData.Ticket;
        }

        public string GetData(string ticket, string parametrs)
        {
            var expectedPercents = double.Parse(parametrs.Split().First());
            var data = new ChangePercentsSubscriptionData
            {
                Ticket = ticket,
                ExpectedPercents = expectedPercents
            };
            return JsonSerializer.Serialize(data);
        }
    }
}