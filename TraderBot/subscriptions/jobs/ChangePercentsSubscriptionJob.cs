using System;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Telegram.Bot;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot
{
    public class ChangePercentsSubscriptionJob : IJob
    {
        private readonly UnionActionPriceService _actionPriceService;
        private readonly ITelegramBotClient bot;
        private readonly EFGenericRepository<Subscription> subscriptions;

        public ChangePercentsSubscriptionJob(UnionActionPriceService actionPriceService, ITelegramBotClient bot,
            EFGenericRepository<Subscription> subscriptions)
        {
            _actionPriceService = actionPriceService;
            this.bot = bot;
            this.subscriptions = subscriptions;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var ticket = context.JobDetail.JobDataMap.GetString("ticket");
            var chatId = context.JobDetail.JobDataMap.GetInt("chatId");
            var expectedPercents = context.JobDetail.JobDataMap.GetDoubleValue("expectedPercents");
            var baseValue = context.JobDetail.JobDataMap.GetDoubleValue("baseValue");
            var destination = context.JobDetail.JobDataMap.GetString("destination");

            var subscriptionId = context.JobDetail.Key;
            var data = await _actionPriceService.GetActionPrice(ticket);

            var change = (data - baseValue) / baseValue * 100;

            if (destination == "up" && change >= expectedPercents ||
                destination == "down" && change <= expectedPercents ||
                Math.Abs(change - expectedPercents) < 0.01)
            {
                await bot.SendTextMessageAsync(chatId, ticket + "Цена тикета изменилась на " + change + "%" +
                                                       "\nТекущая цена: " + data + " руб.");

                var subscription = subscriptions.Get(s => s.CronId == subscriptionId.ToString()).First();
                subscription.Status = 4;
                subscriptions.Save();
                context.Scheduler.DeleteJob(context.JobDetail.Key);
            }
        }
    }
}