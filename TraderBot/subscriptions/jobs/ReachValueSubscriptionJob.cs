using System;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Telegram.Bot;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot
{
    public class ReachValueSubscriptionJob : IJob
    {
        private readonly UnionActionPriceService _actionPriceService;
        private readonly ITelegramBotClient bot;
        private readonly EFGenericRepository<Subscription> subscriptions;

        public ReachValueSubscriptionJob(UnionActionPriceService actionPriceService, ITelegramBotClient bot,
            EFGenericRepository<Subscription> subscriptions)
        {
            _actionPriceService = actionPriceService;
            this.bot = bot;
            this.subscriptions = subscriptions;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var ticket = context.JobDetail.JobDataMap.GetString("ticket");
            var chatId = context.JobDetail.JobDataMap.GetInt("chatId");
            var expectedValue = context.JobDetail.JobDataMap.GetDoubleValue("expectedValue");
            var destination = context.JobDetail.JobDataMap.GetString("destination");

            var subscriptionId = context.JobDetail.Key;
            var data = await _actionPriceService.GetActionPrice(ticket);

            if (destination == "up" && data >= expectedValue ||
                destination == "down" && data <= expectedValue ||
                Math.Abs(expectedValue - data) < expectedValue * 2 / 10000)
            {
                await bot.SendTextMessageAsync(chatId, ticket + "Тикет достиг ожидаемую цену" +
                                                       "\nТекущая цена: " + data + " руб.");

                var subscription = subscriptions.Get(s => s.CronId == subscriptionId.ToString()).First();
                subscription.Status = 4;
                subscriptions.Save();
                context.Scheduler.DeleteJob(context.JobDetail.Key);
            }
        }
    }
}