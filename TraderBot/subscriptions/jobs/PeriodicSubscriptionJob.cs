using System.Collections.Generic;
using System.Threading.Tasks;
using Quartz;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TraderBot.services;

namespace TraderBot
{
    public class PeriodicSubscriptionJob : IJob
    {
        private readonly UnionActionPriceService _actionPriceService;
        private readonly ITelegramBotClient bot;

        public PeriodicSubscriptionJob(UnionActionPriceService actionPriceService, ITelegramBotClient bot)
        {
            _actionPriceService = actionPriceService;
            this.bot = bot;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var ticket = context.JobDetail.JobDataMap.GetString("ticket");
            var chatId = context.JobDetail.JobDataMap.GetInt("chatId");
            var subscriptionId = context.JobDetail.Key;
            var data = await _actionPriceService.GetActionPrice(ticket);

            var buttons = new List<InlineKeyboardButton>
            {
                InlineKeyboardButton.WithCallbackData("Пауза", "SwitchSubscription_" + subscriptionId),

                InlineKeyboardButton.WithCallbackData("Удалить", "DeleteSubscription_" + subscriptionId)
            };

            await bot.SendTextMessageAsync(chatId, ticket + "\nТекущая цена: " + data + " руб.",
                replyMarkup: new InlineKeyboardMarkup(buttons));
        }
    }
}