namespace TraderBot.entities
{
    public class PeriodicSubscriptionData
    {
        public string Ticket { get; set; }
        public int Seconds { get; set; }
        public int Minutes { get; set; }
        public int Hours { get; set; }
    }
}