namespace TraderBot.entities
{
    public class ChangePercentsSubscriptionData
    {
        public string Ticket { get; set; }
        public double ExpectedPercents { get; set; }
    }
}