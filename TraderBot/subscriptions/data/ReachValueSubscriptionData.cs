namespace TraderBot.entities
{
    public class ReachValueSubscriptionData
    {
        public string Ticket { get; set; }
        public double ExpectedValue { get; set; }
    }
}