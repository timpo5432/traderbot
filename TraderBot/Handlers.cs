using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TraderBot.services;

namespace TraderBot
{
    public class Handlers
    {
        public readonly LogicService LogicService;

        public Handlers(LogicService logicService)
        {
            LogicService = logicService;
        }

        public Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception,
            CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException =>
                    $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            return Task.CompletedTask;
        }

        public async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
            CancellationToken cancellationToken)
        {
            var handler = update.Type switch
            {
                UpdateType.Message => BotOnMessageReceived(botClient, update.Message),
                UpdateType.EditedMessage => BotOnMessageReceived(botClient, update.EditedMessage),
                UpdateType.CallbackQuery => BotOnCallbackQueryReceived(botClient, update.CallbackQuery)
            };

            try
            {
                await handler;
            }
            catch (Exception exception)
            {
                await HandleErrorAsync(botClient, exception, cancellationToken);
            }
        }

        private async Task BotOnMessageReceived(ITelegramBotClient botClient, Message message)
        {
            if (message.Type != MessageType.Text) return;

            var entities = (message.Entities ?? Array.Empty<MessageEntity>()).ToList();
            if (entities.Count == 0) LogicService.CommandReceived(botClient, message.From, "", message.Text);

            var entity = entities[0];
            if (entity.Type == MessageEntityType.BotCommand)
            {
                var command = message.Text?.Substring(entity.Offset + 1, entity.Length - 1);
                var data = message.Text.Substring(entity.Offset + entity.Length).Trim();
                LogicService.CommandReceived(botClient, message.From, command, data);
            }
        }

        private async Task BotOnCallbackQueryReceived(ITelegramBotClient botClient, CallbackQuery callbackQuery)
        {
            var commandEnd = callbackQuery.Data.IndexOf("_", StringComparison.Ordinal);
            if (commandEnd == -1) LogicService.CommandReceived(botClient, callbackQuery.From, callbackQuery.Data, "");
            LogicService.CommandReceived(botClient, callbackQuery.From, callbackQuery.Data.Substring(0, commandEnd),
                callbackQuery.Data.Substring(commandEnd + 1, callbackQuery.Data.Length - commandEnd - 1));
        }
    }
}