using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraderBot.entities;
using TraderBot.models;

namespace TraderBot.services
{
    public class UnionActionPriceService
    {
        private static readonly ConcurrentDictionary<string, int> ticketLocks = new();
        private readonly Dictionary<string, IActionPriceService> _actionPriceServices;
        private readonly IActionsDataRepository db;

        private readonly EFGenericRepository<Ticket> tickets;

        public UnionActionPriceService(
            List<IActionPriceService> actionPriceServices,
            EFGenericRepository<Ticket> tickets,
            IActionsDataRepository db)
        {
            this.tickets = tickets;
            this.db = db;
            _actionPriceServices = new Dictionary<string, IActionPriceService>();
            foreach (var actionPriceService in actionPriceServices)
                _actionPriceServices.Add(actionPriceService.Source, actionPriceService);
        }

        public async Task UpdateTicketCandles(Ticket ticket)
        {
            if (ticketLocks.GetOrAdd(ticket.Name, 0) == 1) return;
            ticketLocks[ticket.Name] = 1;
            var a = new MOEXActionPriceService();
            var lastCandle = ticket.LastCandleEndDateTime ?? "2010-01-01";
            var countReaded = 0;
            var candles =
                (await a.GetCandles(ticket.Name, countReaded, DateTime.Parse(lastCandle), CandleInterval.Ten)).ToList();

            while (candles.Count > 0)
            {
                countReaded += candles.Count;
                await db.AddCandles(candles);
                ticket.LastCandleEndDateTime =
                    candles[^1].End.Date.Subtract(TimeSpan.FromDays(1)).ToString("yyyy-MM-dd");
                candles =
                    (await a.GetCandles(ticket.Name, countReaded, DateTime.Parse(lastCandle), CandleInterval.Ten))
                    .ToList();

                tickets.Update(ticket);
                tickets.Save();
                // Console.Out.WriteLine(candles.Count);
            }

            // Console.Out.WriteLine("ended" + ticket.Name + ticket.LastCandleEndDateTime);
            ticketLocks[ticket.Name] = 0;
        }

        public Task<double> GetActionPrice(Ticket ticket)
        {
            var source = ticket.Source;
            return _actionPriceServices[source].GetActionPrice(ticket.Name);
        }

        public Task<double> GetActionPrice(string ticket)
        {
            var source = tickets.Get(t => t.Name.Equals(ticket)).First().Source;
            return _actionPriceServices[source].GetActionPrice(ticket);
        }

        public Task<IEnumerable<Candle>> GetCandles(
            Ticket ticket,
            int start,
            DateTime from,
            CandleInterval interval)
        {
            return _actionPriceServices[ticket.Source].GetCandles(ticket.Name, start, from, interval);
        }

        public Dictionary<string, double> GetPrices(List<Ticket> tickets)
        {
            var result = new Dictionary<string, double>();
            var grouped = tickets.GroupBy(ticket => ticket.Source);
            foreach (var groupedTickets in grouped)
            {
                var groupedResult = _actionPriceServices[groupedTickets.Key]
                    .GetPrices(groupedTickets.Select(ticket => ticket.Name).ToList()).Result;
                foreach (var (ticket, value) in groupedResult) result[ticket] = value;
            }

            return result;
        }

        public async Task<Dictionary<string, List<string>>> GetTicketsList()
        {
            var result = new Dictionary<string, List<string>>();
            foreach (var actionPriceService in _actionPriceServices)
                result[actionPriceService.Key] = await actionPriceService.Value.GetTicketsList();

            return result;
        }
    }
}