using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using TraderBot.entities;

namespace TraderBot.services
{
    public class MOEXActionPriceService : IActionPriceService
    {
        private readonly HttpClient _httpClient = new() {Timeout = TimeSpan.FromSeconds(10)};

        private readonly string baseUrlCandles =
            "http://iss.moex.com/iss/engines/stock/markets/shares/securities/{0}/candles.json?from={1}&interval={2}&start={3}";

        private readonly string baseUrlPrices =
            "https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities.json?securities={0}";

        private readonly string baseUrlTickets =
            "https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities.json";

        private string baseUrl = "http://iss.moex.com/iss/engines/stock/markets/shares/boards/tqbr/securities/{0}.json";

        public MOEXActionPriceService()
        {
            ServicePointManager.DefaultConnectionLimit = 5;
            ServicePointManager.MaxServicePointIdleTime = 1;
        }

        public string Source { get; } = "MOEX";

        public async Task<double> GetActionPrice(string ticketName)
        {
            return GetPrices(new List<string> {ticketName}).Result.First().Value;
        }

        public async Task<IEnumerable<Candle>> GetCandles(string ticketName, int start, DateTime from,
            CandleInterval interval)
        {
            var url = string.Format(baseUrlCandles, ticketName, from.ToString("yyyy-MM-dd"), (int) interval, start);
            // Console.Out.WriteLine(url);

            // var e =  _httpClient.GetAsync(url).Status;
            // Console.Out.WriteLine(e);
            JsonElement root = default;
            JsonDocument doc = default;

            JsonElement data = default;
            try
            {
                var answer = await _httpClient.GetAsync(url);
                var body = await answer.Content.ReadAsStringAsync();
                doc = JsonDocument.Parse(body);

                root = doc.RootElement;

                data = root.GetProperty("candles").GetProperty("data");
            }
            catch (Exception e)
            {
                // Console.Out.WriteLine(e);
                return new List<Candle>();
            }

            var ans = new List<Candle>();

            foreach (var t in data.EnumerateArray())
            {
                var candle = new Candle();
                candle.Ticket = ticketName;
                var values = new List<string>();
                foreach (var val in t.EnumerateArray())
                    if (val.ValueKind == JsonValueKind.Number)
                        values.Add(val.GetDouble().ToString());
                    else
                        values.Add(val.GetString());
                candle.Open = double.Parse(values[0]);
                candle.Close = double.Parse(values[1]);
                candle.High = double.Parse(values[2]);
                candle.Low = double.Parse(values[3]);
                candle.Value = double.Parse(values[4]);
                candle.Volume = double.Parse(values[5]);
                candle.Begin = DateTime.Parse(values[6]);
                candle.End = DateTime.Parse(values[7]);
                ans.Add(candle);
            }

            return ans;
        }

        public async Task<Dictionary<string, double>> GetPrices(List<string> tickets)
        {
            var ans = new Dictionary<string, double>();
            var url = "";
            for (var i = 0; i < tickets.Count; i++)
                if (i % 10 == 9 || i + 1 == tickets.Count)
                {
                    url += tickets[i];
                    Console.Out.WriteLine(string.Format(baseUrlPrices, url));
                    var answer = await _httpClient.GetAsync(string.Format(baseUrlPrices, url));
                    var body = await answer.Content.ReadAsStringAsync();
                    using var doc = JsonDocument.Parse(body);
                    var root = doc.RootElement;
                    var data = root.GetProperty("marketdata").GetProperty("data");
                    foreach (var t in data.EnumerateArray())
                    {
                        var j = 0;
                        var name = "";
                        foreach (var val in t.EnumerateArray())
                        {
                            if (j == 0)
                            {
                                name = val.ToString();
                            }
                            else if (j == 2)
                            {
                                ans[name] = double.Parse(val.GetDouble().ToString());
                                break;
                            }

                            j++;
                        }
                    }

                    url = "";
                }
                else
                {
                    url += tickets[i] + ",";
                }

            return ans;
        }

        public async Task<List<string>> GetTicketsList()
        {
            var answer = await _httpClient.GetAsync(baseUrlTickets);
            var body = await answer.Content.ReadAsStringAsync();
            using var doc = JsonDocument.Parse(body);
            var root = doc.RootElement;
            var data = root.GetProperty("securities").GetProperty("data");
            var ans = new List<string>();
            foreach (var t in data.EnumerateArray())
            foreach (var val in t.EnumerateArray())
            {
                ans.Add(val.ToString());
                break;
            }

            return ans;
        }
    }
}