using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TraderBot.entities;

namespace TraderBot.services
{
    public interface IActionPriceService
    {
        public string Source { get; }

        public Task<double> GetActionPrice(string ticketName);

        public Task<IEnumerable<Candle>> GetCandles(string ticketName, int start, DateTime from,
            CandleInterval interval);

        public Task<Dictionary<string, double>> GetPrices(List<string> tickets);

        public Task<List<string>> GetTicketsList();
    }
}