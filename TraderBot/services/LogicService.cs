using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Stateless;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using TraderBot.entities;
using TraderBot.models;
using TelegramUser = Telegram.Bot.Types.User;

namespace TraderBot.services
{
    public class LogicService
    {
        private readonly Dictionary<string, IPredictor> _predictors = new();
        private readonly IScheduler _scheduler;

        private readonly Dictionary<int, StateMachine<Position, Command>> _stateMachines = new();
        private readonly Dictionary<string, ISubscription> _subscriptions = new();
        private readonly UnionActionPriceService _unionActionPriceService;
        private readonly UnionActionPriceService PriceService;
        private readonly EFGenericRepository<Ticket> tickets;
        private readonly EFGenericRepository<User> users;
        private Dictionary<Command, StateMachine<Position, Command>.TriggerWithParameters> _triggers;

        public LogicService(EFGenericRepository<User> users, UnionActionPriceService priceService,
            EFGenericRepository<Ticket> tickets, IEnumerable<ISubscription> subscriptions, IJobFactory factory,
            IEnumerable<IPredictor> predictors, UnionActionPriceService unionActionPriceService, string connection)
        {
            this.users = users;
            PriceService = priceService;
            this.tickets = tickets;
            _unionActionPriceService = unionActionPriceService;
            foreach (var subscription in subscriptions) _subscriptions[subscription.Name] = subscription;

            foreach (var predictor in predictors) _predictors[predictor.Command] = predictor;

            var prop = new NameValueCollection();

            prop.Add("quartz.jobStore.dataSource", "myDB");
            // prop.Add("quartz.dataSource.name", "myDBBB");
            prop.Add("quartz.jobStore.type", "Quartz.Impl.AdoJobStore.JobStoreTX");
            prop.Add("quartz.serializer.type", "json");
            prop.Add("quartz.jobStore.driverDelegateType", "Quartz.Impl.AdoJobStore.PostgreSQLDelegate");
            prop.Add("quartz.dataSource.myDB.provider", "Npgsql");
            prop.Add("quartz.dataSource.myDB.connectionString", connection);
            var _factory = new StdSchedulerFactory(prop);
            _factory.Initialize(prop);
            _scheduler = _factory.GetScheduler().Result;
            _scheduler.JobFactory = factory;
            _scheduler.Start();
        }

        public void CommandReceived(ITelegramBotClient bot, TelegramUser userInfo, string commandName, string data)
        {
            var user = users.Get(u => u.ChatId == userInfo.Id).Include(u => u.State).Include(u => u.Subscriptions)
                           .FirstOrDefault() ??
                       CreateUser(userInfo);

            var stateMachine = GetStateMachine(bot, user);
            if (commandName != "")
            {
                var command = Command.Help;
                foreach (var commandNameItem in Enum.GetNames<Command>())
                    if (commandNameItem.ToLower().Equals(commandName.ToLower()) || commandNameItem.Equals(commandName))
                    {
                        Enum.TryParse(commandNameItem, out command);
                        break;
                    }

                if (_triggers.ContainsKey(command))
                {
                    stateMachine.Fire(_triggers[command], data);
                    return;
                }

                stateMachine.Fire(command);
                return;
            }

            if (stateMachine.State.ToString().EndsWith("Waiting"))
            {
                stateMachine.Fire(
                    _triggers[
                        stateMachine.PermittedTriggers.First(command => command.ToString().EndsWith("Waiting"))],
                    data);
                return;
            }

            stateMachine.Fire(Command.Help);
        }

        private User CreateUser(TelegramUser userInfo)
        {
            var state = new State
            {
                CurrentPosition = Position.Start.ToString()
            };

            var user = new User
            {
                ChatId = userInfo.Id,
                NickName = userInfo.Username,
                State = state
            };

            users.Create(user);
            users.Save();
            return users.Get(u => u.ChatId == userInfo.Id).FirstOrDefault();
        }

        private StateMachine<Position, Command> GetStateMachine(ITelegramBotClient bot, User user)
        {
            if (_stateMachines.TryGetValue(user.Id, out var stateMachine)) return stateMachine;

            Enum.TryParse<Position>((user.State ?? new State()).CurrentPosition ?? "Start", out var position);
            stateMachine = new StateMachine<Position, Command>(position);
            if (_triggers == null)
                _triggers = new Dictionary<Command, StateMachine<Position, Command>.TriggerWithParameters>();

            var ticketInfoParams =
                stateMachine.SetTriggerParameters<string>(Command.TicketInfoWaiting);

            var subscriptionType =
                stateMachine.SetTriggerParameters<string>(Command.SubscriptionType);

            var subscriptionTypesParams =
                stateMachine.SetTriggerParameters<string>(Command.SubscriptionTypeWaiting);

            var subscriptionWithTicket =
                stateMachine.SetTriggerParameters<string>(Command.AddSubscriptionComplete);

            var predictParams =
                stateMachine.SetTriggerParameters<string>(Command.Predict);

            var switchParameters =
                stateMachine.SetTriggerParameters<string>(Command.SwitchSubscription);

            var deleteParameters =
                stateMachine.SetTriggerParameters<string>(Command.DeleteSubscription);


            _triggers.TryAdd(Command.TicketInfoWaiting, ticketInfoParams);
            _triggers.TryAdd(Command.SubscriptionType, subscriptionType);
            _triggers.TryAdd(Command.SubscriptionTypeWaiting, subscriptionTypesParams);
            _triggers.TryAdd(Command.AddSubscriptionComplete, subscriptionWithTicket);
            _triggers.TryAdd(Command.Predict, predictParams);
            _triggers.TryAdd(Command.SwitchSubscription, switchParameters);
            _triggers.TryAdd(Command.DeleteSubscription, deleteParameters);

            stateMachine.Configure(Position.TicketInfo)
                .OnEntryFrom(ticketInfoParams,
                    ticket => HandleTicketInfoRequest(stateMachine, bot, user, ticket))
                .OnEntryFrom(predictParams,
                    data => HandleDisplayPredict(stateMachine, bot, user, data))
                .PermitReentry(Command.Predict)
                .OnEntryFrom(switchParameters,
                    data => HandleSwitchSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.SwitchSubscription)
                .OnEntryFrom(Command.ListSubscriptions, () => HandleListSubscriptions(stateMachine, bot, user))
                .PermitReentry(Command.ListSubscriptions)
                .OnEntryFrom(deleteParameters, data => HandleDeleteSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.DeleteSubscription)
                .OnEntryFrom(Command.Help, () => HandleHelp(stateMachine, bot, user))
                .PermitReentry(Command.Help)
                .OnEntryFrom(Command.Start, () => HandleStart(stateMachine, bot, user))
                .PermitReentry(Command.Start)
                .Permit(Command.AddSubscription, Position.SubscriptionSettingTicketWaiting)
                .Permit(Command.TicketInfo, Position.TicketInfoWaiting)
                .Permit(Command.AddSubscriptionComplete, Position.SubscriptionSettingType);

            stateMachine.Configure(Position.TicketInfoWaiting)
                .OnEntryFrom(Command.TicketInfo,
                    () => HandleTicketInfoWaitingRequest(stateMachine, bot, user, Position.TicketInfoWaiting))
                .OnEntryFrom(predictParams,
                    data => HandleDisplayPredict(stateMachine, bot, user, data))
                .PermitReentry(Command.Predict)
                .OnEntryFrom(Command.ListSubscriptions, () => HandleListSubscriptions(stateMachine, bot, user))
                .PermitReentry(Command.ListSubscriptions)
                .OnEntryFrom(switchParameters,
                    data => HandleSwitchSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.SwitchSubscription)
                .OnEntryFrom(deleteParameters, data => HandleDeleteSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.DeleteSubscription)
                .OnEntryFrom(Command.Help, () => HandleHelp(stateMachine, bot, user))
                .PermitReentry(Command.Help)
                .OnEntryFrom(Command.Start, () => HandleStart(stateMachine, bot, user))
                .PermitReentry(Command.Start)
                .Permit(Command.TicketInfoWaiting, Position.TicketInfo)
                .Permit(Command.AddSubscription, Position.SubscriptionSettingTicketWaiting)
                .Permit(Command.AddSubscriptionComplete, Position.SubscriptionSettingType)
                .PermitReentry(Command.TicketInfo);

            stateMachine.Configure(Position.Start)
                .OnEntryFrom(predictParams,
                    data => HandleDisplayPredict(stateMachine, bot, user, data))
                .PermitReentry(Command.Predict)
                .OnEntryFrom(Command.ListSubscriptions, () => HandleListSubscriptions(stateMachine, bot, user))
                .PermitReentry(Command.ListSubscriptions)
                .OnEntryFrom(switchParameters,
                    data => HandleSwitchSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.SwitchSubscription)
                .OnEntryFrom(deleteParameters, data => HandleDeleteSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.DeleteSubscription)
                .OnEntryFrom(Command.Help, () => HandleHelp(stateMachine, bot, user))
                .PermitReentry(Command.Help)
                .OnEntryFrom(Command.Start, () => HandleStart(stateMachine, bot, user))
                .PermitReentry(Command.Start)
                .Permit(Command.TicketInfo, Position.TicketInfoWaiting)
                .Permit(Command.AddSubscription, Position.SubscriptionSettingTicketWaiting)
                .Permit(Command.AddSubscriptionComplete, Position.SubscriptionSettingType)
                .PermitReentry(Command.ListSubscriptions);

            stateMachine.Configure(Position.SubscriptionSettingTicketWaiting)
                .OnEntryFrom(Command.AddSubscription,
                    () => HandleTicketInfoWaitingRequest(stateMachine, bot, user,
                        Position.SubscriptionSettingTicketWaiting))
                .OnEntryFrom(predictParams,
                    data => HandleDisplayPredict(stateMachine, bot, user, data))
                .PermitReentry(Command.Predict)
                .OnEntryFrom(Command.ListSubscriptions, () => HandleListSubscriptions(stateMachine, bot, user))
                .PermitReentry(Command.ListSubscriptions)
                .OnEntryFrom(switchParameters,
                    data => HandleSwitchSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.SwitchSubscription)
                .OnEntryFrom(deleteParameters, data => HandleDeleteSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.DeleteSubscription)
                .OnEntryFrom(Command.Help, () => HandleHelp(stateMachine, bot, user))
                .PermitReentry(Command.Help)
                .OnEntryFrom(Command.Start, () => HandleStart(stateMachine, bot, user))
                .PermitReentry(Command.Start)
                .Permit(Command.TicketInfo, Position.TicketInfoWaiting)
                .Permit(Command.AddSubscriptionComplete, Position.SubscriptionSettingType)
                .PermitReentry(Command.AddSubscription)
                .Permit(Command.TicketInfoWaiting, Position.SubscriptionSettingType);


            stateMachine.Configure(Position.SubscriptionSettingType)
                .OnEntryFrom(ticketInfoParams,
                    data => HandleSubscriptionTypeWaitingRequest(stateMachine, bot, user, data))
                .OnEntryFrom(subscriptionWithTicket,
                    data => HandleSubscriptionTypeWaitingRequest(stateMachine, bot, user, data))
                .OnEntryFrom(Command.ListSubscriptions, () => HandleListSubscriptions(stateMachine, bot, user))
                .PermitReentry(Command.ListSubscriptions)
                .OnEntryFrom(predictParams,
                    data => HandleDisplayPredict(stateMachine, bot, user, data))
                .PermitReentry(Command.Predict)
                .OnEntryFrom(switchParameters,
                    data => HandleSwitchSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.SwitchSubscription)
                .OnEntryFrom(deleteParameters, data => HandleDeleteSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.DeleteSubscription)
                .OnEntryFrom(Command.Help, () => HandleHelp(stateMachine, bot, user))
                .PermitReentry(Command.Help)
                .Permit(Command.AddSubscription, Position.SubscriptionSettingTicketWaiting)
                .Permit(Command.TicketInfo, Position.TicketInfoWaiting)
                .PermitReentry(Command.AddSubscriptionComplete)
                .OnEntryFrom(Command.Start, () => HandleStart(stateMachine, bot, user))
                .PermitReentry(Command.Start)
                .Permit(Command.SubscriptionType, Position.SubscriptionSettingParamsWaiting);

            stateMachine.Configure(Position.SubscriptionSettingParamsWaiting)
                .OnEntryFrom(subscriptionType,
                    data => HandleSubscriptionTypeChoosen(stateMachine, bot, user, data))
                .OnEntryFrom(predictParams,
                    data => HandleDisplayPredict(stateMachine, bot, user, data))
                .PermitReentry(Command.Predict)
                .OnEntryFrom(switchParameters,
                    data => HandleSwitchSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.SwitchSubscription)
                .OnEntryFrom(Command.ListSubscriptions, () => HandleListSubscriptions(stateMachine, bot, user))
                .PermitReentry(Command.ListSubscriptions)
                .OnEntryFrom(deleteParameters, data => HandleDeleteSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.DeleteSubscription)
                .OnEntryFrom(Command.Help, () => HandleHelp(stateMachine, bot, user))
                .PermitReentry(Command.Help)
                .OnEntryFrom(Command.Start, () => HandleStart(stateMachine, bot, user))
                .PermitReentry(Command.Start)
                .Permit(Command.SubscriptionTypeWaiting, Position.SubscriptionCreated)
                .Permit(Command.AddSubscriptionComplete, Position.SubscriptionSettingType)
                .Permit(Command.AddSubscription, Position.SubscriptionSettingTicketWaiting)
                .Permit(Command.TicketInfo, Position.TicketInfoWaiting);

            stateMachine.Configure(Position.SubscriptionCreated)
                .OnEntryFrom(subscriptionTypesParams,
                    data => HandleSubscriptionCreation(stateMachine, bot, user, data))
                .OnEntryFrom(predictParams,
                    data => HandleDisplayPredict(stateMachine, bot, user, data))
                .PermitReentry(Command.Predict)
                .OnEntryFrom(switchParameters,
                    data => HandleSwitchSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.SwitchSubscription)
                .OnEntryFrom(Command.ListSubscriptions, () => HandleListSubscriptions(stateMachine, bot, user))
                .PermitReentry(Command.ListSubscriptions)
                .OnEntryFrom(deleteParameters, data => HandleDeleteSubscription(stateMachine, bot, user, data))
                .PermitReentry(Command.DeleteSubscription)
                .OnEntryFrom(Command.Help, () => HandleHelp(stateMachine, bot, user))
                .PermitReentry(Command.Help)
                .OnEntryFrom(Command.Start, () => HandleStart(stateMachine, bot, user))
                .PermitReentry(Command.Start)
                .Permit(Command.AddSubscription, Position.SubscriptionSettingTicketWaiting)
                .Permit(Command.TicketInfo, Position.TicketInfoWaiting)
                .Permit(Command.AddSubscriptionComplete, Position.SubscriptionSettingType);

            _stateMachines.TryAdd(user.Id, stateMachine);

            return stateMachine;
        }

        private async void HandleHelp(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user)
        {
            const string help = @"
/listsubscriptions - вывести список подписок
/help - показать помощь
/ticketinfo - вывести текущую цену тикета
/addsubscription - начать добавление подписки
/help - вывести помощь
/start - описание бота
";
            bot.SendTextMessageAsync(user.ChatId, help);
        }

        private async void HandleStart(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user)
        {
            const string help = @"
Привет, я бот, который умеет предсказывать цены акций и уведомлять об изменениях на рынке
Если хотите узнать список моих команд, напишите /help
";
            bot.SendTextMessageAsync(user.ChatId, help);
        }

        private async void HandleSwitchSubscription(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user, string data)
        {
            var subscription = user.Subscriptions.FirstOrDefault(s => s.CronId == data && s.Status != 4);
            if (subscription == null)
            {
                await bot.SendTextMessageAsync(user.ChatId, "Подписка не существует");
                return;
            }

            InlineKeyboardButton button = null;

            if (subscription.Status == 1)
            {
                subscription.Status = 2;
                var splitted = subscription.CronId.Split(".");
                await _scheduler.PauseJob(new JobKey(splitted[1], splitted[0]));
                users.Save();
                button = InlineKeyboardButton.WithCallbackData("Возобновить",
                    "SwitchSubscription_" + subscription.CronId);
            }

            else if (subscription.Status == 2)
            {
                subscription.Status = 1;
                var splitted = subscription.CronId.Split(".");
                await _scheduler.ResumeJob(new JobKey(splitted[1], splitted[0]));
                users.Save();
                button = InlineKeyboardButton.WithCallbackData("Пауза",
                    "SwitchSubscription_" + subscription.CronId);
            }

            var buttons = new List<InlineKeyboardButton>();
            if (button != null)

                buttons.Add(button);

            buttons.Add(
                InlineKeyboardButton.WithCallbackData("Удалить", "DeleteSubscription_" + subscription.CronId));

            var inlineKeyboard = new InlineKeyboardMarkup(buttons);
            await bot.SendTextMessageAsync(user.ChatId,
                (subscription.Status == 1
                    ? "Активная"
                    : "Остановлена") + ". " + _subscriptions[subscription.Type].GetDescription(subscription.Data),
                replyMarkup: inlineKeyboard);
        }

        private async void HandleDeleteSubscription(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user, string data)
        {
            var subscription = user.Subscriptions.FirstOrDefault(s => s.CronId == data && s.Status != 4);
            if (subscription == null)
            {
                await bot.SendTextMessageAsync(user.ChatId, "Подписка не существует");
                return;
            }

            subscription.Status = 4;
            var splitted = subscription.CronId.Split(".");
            await _scheduler.DeleteJob(new JobKey(splitted[1], splitted[0]));
            users.Save();
            await bot.SendTextMessageAsync(user.ChatId, "Подписка успешно удалена");
        }

        private async void HandleDisplayPredict(StateMachine<Position, Command> stateMachine, ITelegramBotClient bot,
            User user, string data)
        {
            var dataSplitted = data.Split("_");
            var predictor = _predictors[dataSplitted[0]];
            var ticket = dataSplitted[1];
            await _unionActionPriceService.UpdateTicketCandles(tickets.Get(ticket1 => ticket1.Name.Equals(ticket))
                .First());

            var predict = predictor.GetExpectedPrice(ticket, 60);

            await bot.SendTextMessageAsync(user.ChatId,
                ticket + " " + predictor.Name + "\n Предполагаемая цена через 60 минут: " + predict + " руб.");
        }

        private async void HandleListSubscriptions(StateMachine<Position, Command> stateMachine, ITelegramBotClient bot,
            User user)
        {
            var activeSUbscriptions = user.Subscriptions.FindAll(s => s.Status != 4);
            if (activeSUbscriptions.Count == 0)
                bot.SendTextMessageAsync(user.ChatId, "У вас пока что нет подписок(\n создать? /addsubscription");

            foreach (var subscription in activeSUbscriptions)
            {
                InlineKeyboardButton button = null;
                if (subscription.Status == 1)
                    button = InlineKeyboardButton.WithCallbackData("Пауза",
                        "SwitchSubscription_" + subscription.CronId);
                else if (subscription.Status == 2)
                    button = InlineKeyboardButton.WithCallbackData("Возобновить",
                        "SwitchSubscription_" + subscription.CronId);

                var buttons = new List<InlineKeyboardButton>();
                if (button != null)

                    buttons.Add(button);

                buttons.Add(
                    InlineKeyboardButton.WithCallbackData("Удалить", "DeleteSubscription_" + subscription.CronId));

                var inlineKeyboard = new InlineKeyboardMarkup(buttons);
                await bot.SendTextMessageAsync(user.ChatId,
                    (subscription.Status == 1
                        ? "Активная"
                        : "Остановлена") + ". " + _subscriptions[subscription.Type].GetDescription(subscription.Data),
                    replyMarkup: inlineKeyboard);
            }
        }

        private async void HandleSubscriptionCreation(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user, string subscriptionParams)
        {
            var chatData = JsonSerializer.Deserialize<ChatData>(user.State.Data);
            var job = _subscriptions[chatData.SubscriptionType].CreateJob(chatData.Ticket, subscriptionParams, user);
            var trigger = _subscriptions[chatData.SubscriptionType]
                .CreateTrigger(chatData.Ticket, subscriptionParams, user);
            if (job == null || trigger == null)
            {
                await bot.SendTextMessageAsync(user.ChatId, "Wrong parametrs");
                return;
            }
            var subscription = new Subscription
            {
                CronId = job.Key.ToString(),
                Status = 1,
                Type = chatData.SubscriptionType,
                Data = _subscriptions[chatData.SubscriptionType].GetData(chatData.Ticket, subscriptionParams)
            };
            user.Subscriptions.Add(subscription);
            users.Save();
            await _scheduler.ScheduleJob(job, trigger);
            bot.SendTextMessageAsync(user.ChatId, "Подписка создана!\nпосмотреть все подписки: /listsubscriptions");
        }

        private async void HandleSubscriptionTypeChoosen(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user, string subscriptionType)
        {
            var data = JsonSerializer.Deserialize<ChatData>(user.State.Data);
            data.SubscriptionType = subscriptionType;
            user.State.Data = JsonSerializer.Serialize(data);
            users.Save();
            await bot.SendTextMessageAsync(user.ChatId, _subscriptions[subscriptionType].ParamsHelpMessage);
        }

        private async void HandleTicketInfoWaitingRequest(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user, Position position)
        {
            // user.State.CurrentPosition = position.ToString();
            user.State.CurrentPosition = stateMachine.State.ToString();
            users.Save();
            await bot.SendTextMessageAsync(user.ChatId, "Введите название тикета");
        }

        private async void HandleSubscriptionTypeWaitingRequest(StateMachine<Position, Command> stateMachine,
            ITelegramBotClient bot, User user, string ticketName)
        {
            ticketName = ticketName.ToUpper();
            var ticket = tickets.Get(ticket => ticket.Name.ToUpper().Equals(ticketName)).FirstOrDefault();
            if (ticket == null)
            {
                await bot.SendTextMessageAsync(user.ChatId, "тикет не найден");
                return;
            }

            user.State.CurrentPosition = stateMachine.State.ToString();
            users.Save();
            var array = new List<List<InlineKeyboardButton>>();
            foreach (var (_, subscription) in _subscriptions)
                array.Add(new List<InlineKeyboardButton>
                {
                    InlineKeyboardButton.WithCallbackData(subscription.Description,
                        "SubscriptionType_" + subscription.Name)
                });

            var inlineKeyboard = new InlineKeyboardMarkup(array);
            user.State.Data = JsonSerializer.Serialize(new ChatData {Ticket = ticketName});
            users.Update(user);
            users.Save();

            await bot.SendTextMessageAsync(user.ChatId, "Выберите тип подписки",
                replyMarkup: inlineKeyboard);
        }

        private async void HandleTicketInfoRequest(StateMachine<Position, Command> stateMachine, ITelegramBotClient bot,
            User user, StateMachine<Position, Command>.Transition ticketTransition)
        {
            var ticketName = (string) ticketTransition.Parameters[0];
            ticketName = ticketName.ToUpper();
            var ticket = tickets.Get(ticket => ticket.Name.Equals(ticketName)).FirstOrDefault();
            Task<double> actionPrice;
            if (ticket == null || (actionPrice = PriceService.GetActionPrice(ticket)) == null)
            {
                await bot.SendTextMessageAsync(user.ChatId, "тикет не найден");
                return;
            }

            var data = await actionPrice;
            var keyboard = new List<List<InlineKeyboardButton>>();
            keyboard.Add(new List<InlineKeyboardButton>
            {
                InlineKeyboardButton.WithCallbackData("Добавить подписку", "AddSubscriptionComplete_" + ticketName)
            });
            var predictors = _predictors.Values.ToList();
            for (var i = 0; i < predictors.Count; i += 2)
            {
                var line = new List<InlineKeyboardButton>
                {
                    InlineKeyboardButton.WithCallbackData(predictors[i].Name,
                        "Predict_" + predictors[i].Command + "_" + ticketName)
                };
                if (i + 1 < predictors.Count)
                    line.Add(InlineKeyboardButton.WithCallbackData(predictors[i + 1].Name,
                        "Predict_" + predictors[i + 1].Command + "_" + ticketName));

                keyboard.Add(line);
            }

            var inlineKeyboard = new InlineKeyboardMarkup(keyboard);

            user.State.Data = JsonSerializer.Serialize(new ChatData {Ticket = ticketName});
            users.Update(user);
            users.Save();

            await bot.SendTextMessageAsync(user.ChatId, ticketName + "\nТекущая цена: " + data + " руб.",
                replyMarkup: inlineKeyboard);
        }
    }
}