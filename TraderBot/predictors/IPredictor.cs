﻿namespace TraderBot.entities
{
    public interface IPredictor
    {
        string Name { get; }
        string Command { get; }
        string Help { get; }
        public double GetExpectedPrice(string ticket, int periodInMinutes);
    }
}