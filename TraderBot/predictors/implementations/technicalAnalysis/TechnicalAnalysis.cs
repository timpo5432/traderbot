﻿using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace TraderBot.entities
{
    public enum Advice
    {
        ActivelySell,
        Sell,
        Hold,
        Buy,
        ActivelyBuy
    }

    public static class TechnicalAnalysis
    {
        private const long ticksPerMinute = 10 * 1000 * 1000 * 60;

        public static IEnumerable<Candle> GetLocalMinimums(Chart chart, int region)
        {
            yield return chart[0];
            for (var i = region; i + region < chart.Count; i++)
                if (chart.GetMinimumInRange(i - region, i + region).Equals(chart[i]))
                    yield return chart[i];
        }

        public static IEnumerable<Candle> GetLocalMaximums(Chart chart, int region)
        {
            yield return chart[0];
            for (var i = region; i + region < chart.Count; i++)
                if (chart.GetMaximumInRange(i - region, i + region).Equals(chart[i]))
                    yield return chart[i];
        }

        public static (double k, double b) GetTrendLineEquation(IEnumerable<Candle> candles, Func<Candle, double> func)
        {
            var times = new List<double>();
            var values = new List<double>();
            var firstCandle = new Candle(DateTime.MinValue, DateTime.MinValue);
            foreach (var candle in candles)
                if (firstCandle.Begin == DateTime.MinValue)
                {
                    firstCandle = candle;
                }
                else
                {
                    times.Add(Convert.ToInt32(candle.Begin.Subtract(firstCandle.Begin).TotalMinutes));
                    values.Add(func(candle));
                }

            if (times.Count <= 1) return (0, 0);
            var matrix = new double[times.Count, 2];
            for (var i = 0; i < times.Count; i++)
            {
                matrix[i, 0] = times[i];
                matrix[i, 1] = 1;
            }

            var A = Matrix<double>.Build.DenseOfArray(matrix);
            var ATransposed = A.Transpose();
            A = ATransposed * A;
            var b = Vector<double>.Build.Dense(values.ToArray());
            b = ATransposed * b;
            var solution = A.Solve(b);
            return (solution[0], solution[1]);
        }

        public static (double k, double b) GetTrendLineEquation(Chart chart)
        {
            var candles = new List<Candle>();
            for (var i = 0; i < chart.Count; i++) candles.Add(chart[i]);
            return GetTrendLineEquation(candles, candle => candle.Close);
        }

        public static (double k, double b) GetSupportLine(Chart chart)
        {
            return GetTrendLineEquation(GetLocalMinimums(chart, 10), candle => candle.Low);
        }

        public static (double k, double b) GetResistanceLine(Chart chart)
        {
            return GetTrendLineEquation(GetLocalMaximums(chart, 10), candle => candle.High);
        }

        public static bool IsUptrend(Chart chart)
        {
            return GetSupportLine(chart).k > 0.02 && GetResistanceLine(chart).k > 0.02;
        }

        public static bool IsDowntrend(Chart chart)
        {
            return GetSupportLine(chart).k < -0.02 && GetResistanceLine(chart).k < -0.02;
        }

        public static bool IsFlatTrend(Chart chart)
        {
            return Math.Abs(GetSupportLine(chart).k) < 0.02 && Math.Abs(GetResistanceLine(chart).k) < 0.02;
        }

        public static List<double> GetMovingAverage(Chart chart, int period)
        {
            var movingAverage = new List<double>();
            if (chart.Count < period)
            {
                for (var i = 0; i < chart.Count; i++) movingAverage.Add(-1);
                return movingAverage;
            }

            var sum = 0.0;
            for (var i = 0; i < period; i++)
            {
                sum += chart[i].Close;
                if (i + 1 != period) movingAverage.Add(-1);
            }

            movingAverage.Add(sum / period);
            for (var i = period; i < chart.Count; i++)
            {
                sum += chart[i].Close;
                sum -= chart[i - period].Close;
                movingAverage.Add(sum / period);
            }

            return movingAverage;
        }

        public static Advice GetAdvice(Chart chart)
        {
            if (IsFlatTrend(chart)) return Advice.Hold;

            var movingAverage = GetMovingAverage(chart, 10);
            var trendDuration = 0;
            var difference = 0.0;
            for (var i = movingAverage.Count - 1; i >= 0; i--)
                if (movingAverage[i] < chart[i].Close ==
                    movingAverage[^1] < chart[^1].Close)
                {
                    trendDuration++;
                    if (Math.Abs(movingAverage[i] - -1) > 0.000001) difference += chart[i].Close - movingAverage[i];
                }
                else
                {
                    break;
                }

            var sameTrends = IsUptrend(chart) && chart[^1].Close > movingAverage[^1] ||
                             IsDowntrend(chart) && chart[^1].Close < movingAverage[^1];
            if (trendDuration >= 7)
            {
                if (sameTrends)
                {
                    if (Math.Abs(difference) / trendDuration >= 0.05 * chart[^1].Close ||
                        GetTrendLineEquation(chart).k > 0.1)
                        return IsUptrend(chart) ? Advice.ActivelyBuy : Advice.ActivelySell;
                    return IsUptrend(chart) ? Advice.Buy : Advice.Sell;
                }

                if (chart[^1].Close > movingAverage[^1])
                    return Advice.Buy;
                if (chart[^1].Close < movingAverage[^1]) return Advice.Sell;
            }
            else if (IsUptrend(chart))
            {
                return Advice.Buy;
            }
            else if (IsDowntrend(chart))
            {
                return Advice.Sell;
            }

            return Advice.Hold;
        }
    }
}