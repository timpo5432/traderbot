﻿using System;

namespace TraderBot.entities
{
    public class TechnicalAnalysisPredictor : IPredictor
    {
        private readonly IActionsDataRepository influxDbActionsRepository;

        public TechnicalAnalysisPredictor(IActionsDataRepository influxDbActionsRepository)
        {
            this.influxDbActionsRepository = influxDbActionsRepository;
        }

        public string Name => "прогноз теханализ";
        public string Command => "FutureTechnical";
        public string Help => "получить прогноз цены с помощью технического анализа";

        public double GetExpectedPrice(string ticket, int periodInMinutes)
        {
            try
            {
                var chart = new Chart(influxDbActionsRepository.GetCandles(ticket,
                    DateTime.Today.Subtract(TimeSpan.FromDays(1)), DateTime.Now, periodInMinutes));

                var trendLine = TechnicalAnalysis.GetTrendLineEquation(chart);
                var movingAverage = TechnicalAnalysis.GetMovingAverage(chart, 10);

                var trendDuration = 0;
                var difference = 0.0;
                for (var i = movingAverage.Count - 1; i >= 0; i--)
                    if (movingAverage[i] < chart[i].Close ==
                        movingAverage[^1] < chart[^1].Close)
                    {
                        trendDuration++;
                        if (Math.Abs(movingAverage[i] - -1) > 0.000001) difference += chart[i].Close - movingAverage[i];
                    }
                    else
                    {
                        break;
                    }

                return chart[^1].Close + difference / trendDuration * 0.2 + trendLine.k * 0.8;
            }
            catch
            {
                return -1;
            }
        }
    }
}