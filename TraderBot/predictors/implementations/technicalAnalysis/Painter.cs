﻿using System;
using System.Drawing;
using System.Globalization;

namespace TraderBot.entities
{
    public static class Painter
    {
        public static string SaveImage(Bitmap bitmap, string path = ".")
        {
            var filename = DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/', '-').Replace(".", "-")
                .Replace(" ", "-").Replace(":", "-") + ".png";
            bitmap.Save(path + "\\" + filename);
            return filename;
        }

        private static int ConvertYCoordinate(double y, double minValue, double maxValue, int bitmapHeight)
        {
            var coefficient = bitmapHeight / (int) (maxValue - minValue + 1);
            return (int) (bitmapHeight - coefficient * (y - minValue));
        }

        public static Bitmap GetBitmapFromChart(Chart chart, int bitmapWidth = 1024, int bitmapHeight = 768)
        {
            var bitmap = new Bitmap(bitmapWidth, bitmapHeight);

            var candleWidth = bitmapWidth / (chart.Count + 2);
            var minValue = chart[0].High;
            var maxValue = chart[0].Low;
            for (var i = 1; i < chart.Count; i++)
            {
                minValue = Math.Min(minValue, chart[i].Low);
                maxValue = Math.Max(maxValue, chart[i].High);
            }

            var redPen = new Pen(Color.IndianRed, candleWidth / 3);
            var greenPen = new Pen(Color.PaleGreen, candleWidth / 3);
            var redBrush = new SolidBrush(Color.IndianRed);
            var greenBrush = new SolidBrush(Color.PaleGreen);

            using var graphics = Graphics.FromImage(bitmap);
            for (var i = 0; i < chart.Count; i++)
                if (chart[i].Open <= chart[i].Close)
                {
                    graphics.DrawLine(greenPen, (i + 1) * candleWidth + candleWidth / 2,
                        ConvertYCoordinate(chart[i].High, minValue, maxValue, bitmapHeight),
                        (i + 1) * candleWidth + candleWidth / 2,
                        ConvertYCoordinate(chart[i].Low, minValue, maxValue, bitmapHeight));
                    graphics.FillRectangle(greenBrush, (i + 1) * candleWidth,
                        ConvertYCoordinate(chart[i].Close, minValue, maxValue, bitmapHeight), candleWidth - 1,
                        ConvertYCoordinate(chart[i].Open, minValue, maxValue, bitmapHeight) -
                        ConvertYCoordinate(chart[i].Close, minValue, maxValue, bitmapHeight) + 1);
                }
                else
                {
                    graphics.DrawLine(redPen, (i + 1) * candleWidth + candleWidth / 2,
                        ConvertYCoordinate(chart[i].High, minValue, maxValue, bitmapHeight),
                        (i + 1) * candleWidth + candleWidth / 2,
                        ConvertYCoordinate(chart[i].Low, minValue, maxValue, bitmapHeight));
                    graphics.FillRectangle(redBrush, (i + 1) * candleWidth,
                        ConvertYCoordinate(chart[i].Open, minValue, maxValue, bitmapHeight), candleWidth - 1,
                        ConvertYCoordinate(chart[i].Close, minValue, maxValue, bitmapHeight) -
                        ConvertYCoordinate(chart[i].Open, minValue, maxValue, bitmapHeight) + 1);
                }

            return bitmap;
        }

        public static void DrawLineOnBitmap(Bitmap bitmap, Chart chart, (double k, double b) line, Color color)
        {
            var candleWidth = bitmap.Width / (chart.Count + 2);
            var minValue = chart[0].High;
            var maxValue = chart[0].Low;
            for (var i = 1; i < chart.Count; i++)
            {
                minValue = Math.Min(minValue, chart[i].Low);
                maxValue = Math.Max(maxValue, chart[i].High);
            }

            using var graphics = Graphics.FromImage(bitmap);
            graphics.DrawLine(new Pen(color, candleWidth / 3), candleWidth,
                ConvertYCoordinate(line.k * chart[0].DurationInMinutes + line.b, minValue, maxValue, bitmap.Height),
                (chart.Count + 1) * candleWidth,
                ConvertYCoordinate(line.k * (chart.Count + 1) * chart[0].DurationInMinutes + line.b, minValue, maxValue,
                    bitmap.Height));
        }
    }
}