﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TraderBot.entities
{
    public class Chart : IEnumerable<Candle>
    {
        private readonly SparseTable<Candle> minimumTable, maximumTable;

        public Chart(IEnumerable<Candle> candles)
        {
            Count = candles.ToList().Count;
            maximumTable = new SparseTable<Candle>(candles.ToList(), MathExtensions.Max);
            minimumTable = new SparseTable<Candle>(candles.ToList(), MathExtensions.Min);
        }

        public int Count { get; }

        public Candle this[int id] => minimumTable.GetValue(id, id);

        IEnumerator<Candle> IEnumerable<Candle>.GetEnumerator()
        {
            return maximumTable.GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return maximumTable.GetEnumerator();
        }

        public Candle GetMinimumInRange(int left, int right)
        {
            return minimumTable.GetValue(left, right);
        }

        public Candle GetMaximumInRange(int left, int right)
        {
            return maximumTable.GetValue(left, right);
        }
    }
}