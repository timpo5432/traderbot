﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TraderBot.entities
{
    public class SparseTableEnumerator<T> : IEnumerator<T>
    {
        private readonly SparseTable<T> sparseTable;
        private int i;

        public SparseTableEnumerator(SparseTable<T> sparseTable)
        {
            this.sparseTable = sparseTable;
            i = -1;
        }

        public bool MoveNext()
        {
            if (i + 1 < sparseTable.Count)
            {
                i++;
                return true;
            }

            return false;
        }

        public void Reset()
        {
        }

        public T Current => sparseTable[i];

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }
    }

    public class SparseTable<T> : IEnumerable<T>
    {
        private readonly int log;
        private readonly int[] maximumLog;
        private readonly Func<T, T, T> operation;
        private readonly T[,] table;

        public SparseTable(IReadOnlyList<T> array, Func<T, T, T> operation)
        {
            Count = array.Count;
            this.operation = operation;
            log = 0;
            while (1 << log <= array.Count) log++;
            table = new T[log, array.Count];
            maximumLog = new int[array.Count + 1];
            if (array.Count != 0) maximumLog[1] = 0;
            for (var i = 2; i < array.Count + 1; i++) maximumLog[i] = maximumLog[i / 2] + 1;
            for (var i = 0; i < array.Count; i++) table[0, i] = array[i];

            for (var level = 1; level < log; level++)
            for (var i = 0; i + (1 << level) - 1 < array.Count; i++)
                table[level, i] = operation(table[level - 1, i], table[level - 1, i + (1 << (level - 1))]);
        }

        public T this[int id] => GetValue(id, id);

        public int Count { get; }

        public IEnumerator<T> GetEnumerator()
        {
            return new SparseTableEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public T GetValue(int left, int right)
        {
            if (left > right) throw new ArgumentException("left border can't be larger than right");
            if (left < 0) throw new ArgumentException("left border can't be less than zero");

            if (right >= maximumLog.Length - 1)
                throw new ArgumentException("right border can't be more than array size");
            var length = maximumLog[right - left + 1];
            return operation(table[length, left], table[length, right - (1 << length) + 1]);
        }
    }
}