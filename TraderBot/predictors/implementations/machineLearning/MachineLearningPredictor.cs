﻿using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace TraderBot.entities
{
    public class MachineLearningPredictor : IPredictor
    {
        private readonly HttpClient _httpClient = new();
        private readonly string url;

        public MachineLearningPredictor(string url)
        {
            this.url = url + "/predict/";
        }

        public string Name => "предсказание машинкой";
        public string Command => "FutureMachine";
        public string Help => "Получить прогноз цены тикета с помощью машинного обучения(махания)";

        public double GetExpectedPrice(string ticket, int periodInMinutes)
        {
            try
            {
                return ParseAnswer(ticket, periodInMinutes).Result;
            }
            catch
            {
                return -1;
            }
        }

        private async Task<double> ParseAnswer(string ticket, int periodInMinutes)
        {
            var answer = await _httpClient.GetAsync(url + ticket + "/" + periodInMinutes);
            var body = await answer.Content.ReadAsStringAsync();
            return double.Parse(body, CultureInfo.InvariantCulture);
        }
    }
}