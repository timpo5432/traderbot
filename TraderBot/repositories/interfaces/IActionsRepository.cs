using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TraderBot.entities;

namespace TraderBot
{
    public interface IActionsDataRepository
    {
        public Task AddCandles(IEnumerable<Candle> candles);
        public IEnumerable<Candle> GetCandles(string ticket, DateTime start, DateTime stop, int periodInMinutes);
    }
}