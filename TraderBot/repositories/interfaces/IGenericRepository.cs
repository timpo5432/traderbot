using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TraderBot
{
    public interface IGenericRepository<TEntity> : IDisposable where TEntity : class
    {
        void Create(TEntity item);
        IEnumerable<TEntity> Get();
        TEntity Get(int id);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        void Delete(Expression<Func<TEntity, bool>> predicate);
        void Delete(int id);
        void Update(TEntity item);
        void Save();
    }
}