using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using NodaTime;
using TraderBot.entities;

namespace TraderBot
{
    public class InfluxDbActionsRepository : IActionsDataRepository
    {
        private static readonly List<(string field, string function)> fields = new()
        {
            ("open", "first"), ("close", "last"), ("high", "max"), ("low", "min"), ("value", "mean"), ("volume", "sum")
        };

        private readonly InfluxDBClient _client;
        private readonly WriteApiAsync _writeApiAsync;
        private readonly string bucket;
        private readonly string org;

        public InfluxDbActionsRepository(string connectionString)
        {
            var data = connectionString.Split(";");
            bucket = data[1];
            org = data[2];

            _client = InfluxDBClientFactory.Create("http://" + data[0], data[3]);
            _writeApiAsync = _client.GetWriteApiAsync();
        }

        public async Task AddCandles(IEnumerable<Candle> candles)
        {
            using (var writeApi = _client.GetWriteApi())
            {
                var points = new List<PointData>();
                foreach (var candle in candles)
                {
                    var point = PointData
                        .Measurement("candles")
                        .Tag("ticket", candle.Ticket)
                        .Field("open", candle.Open)
                        .Field("close", candle.Close)
                        .Field("high", candle.High)
                        .Field("low", candle.Low)
                        .Field("value", candle.Value)
                        .Field("volume", candle.Volume)
                        .Timestamp(candle.Begin.ToUniversalTime(), WritePrecision.S);
                    points.Add(point);
                }

                writeApi.WritePoints(bucket, org, points);
            }
        }

        public IEnumerable<Candle> GetCandles(string ticket, DateTime start, DateTime stop, int periodInMinutes)
        {
            var candlesData = new Dictionary<DateTime, Dictionary<string, double>>();

            foreach (var field in fields)
            foreach (var fieldValue in GetFieldsViaFunction(ticket, start, stop, periodInMinutes, field.field,
                field.function))
            {
                if (!candlesData.ContainsKey(fieldValue.start))
                    candlesData[fieldValue.start] = new Dictionary<string, double>();
                candlesData[fieldValue.start][field.field] = fieldValue.field;
            }

            foreach (var candle in candlesData)
                if (candle.Value.ContainsKey("open") && candle.Value.ContainsKey("close") &&
                    candle.Value.ContainsKey("low") && candle.Value.ContainsKey("high") &&
                    candle.Value.ContainsKey("value") && candle.Value.ContainsKey("volume"))
                    yield return new Candle(candle.Key, candle.Key.Add(TimeSpan.FromMinutes(periodInMinutes)),
                        candle.Value["open"], candle.Value["close"], candle.Value["low"],
                        candle.Value["high"], candle.Value["value"], candle.Value["volume"]);
        }

        private static string DateTimeToFluxFormat(DateTime dateTime)
        {
            return
                $"{dateTime.Year}-{dateTime.Month:d2}-{dateTime.Day:d2}T{dateTime.Hour:d2}:{dateTime.Minute:d2}:{dateTime.Second:d2}Z";
        }

        private IEnumerable<(DateTime start, double field)> GetFieldsViaFunction(string ticket, DateTime start,
            DateTime stop,
            int periodInMinutes, string field, string function)
        {
            var query =
                $"from(bucket: \"{bucket}\") |> range(start: {DateTimeToFluxFormat(start)}, stop: {DateTimeToFluxFormat(stop)}) |> filter(fn: (r) => r._measurement == \"candles\" and r.ticket == \"{ticket}\" and r._field == \"{field}\") |> window(every: {periodInMinutes}m) |> {function}()";
            var tables = _client.GetQueryApi().QueryAsync(query, bucket).Result;

            foreach (var record in tables.SelectMany(table => table.Records))
                yield return (((Instant) record.Values["_start"]).ToDateTimeUtc(), (double) record.Values["_value"]);
        }

        public async Task Test()
        {
            var query = "from(bucket: \"traderbot\")" +
                        " |> range(start: -330d, stop: -10d)" +
                        " |> filter(fn: (r) => r[\"_measurement\"] == \"candles\" and r[\"_field\"] == \"open\" and r[\"ticket\"] ==\"gazp\")";
            var s = new Stopwatch();
            s.Start();
            var tables = await _client.GetQueryApi().QueryAsync(query, org);
            s.Stop();
            // Console.WriteLine(s.ElapsedMilliseconds);
            foreach (var record in tables.SelectMany(table => table.Records))
            {
                // if (record.Values["_field"].Equals("close"))
                // {
                //     Console.WriteLine(record.Values["_value"]);
                // }
                // Console.WriteLine(record.Values["_field"]);
                //Console.WriteLine(record.Values["_value"]);
                //Console.WriteLine($"{record}");
            }
        }
    }
}