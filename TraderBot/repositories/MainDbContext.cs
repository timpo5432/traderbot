using Microsoft.EntityFrameworkCore;
using TraderBot.models;

namespace TraderBot
{
    public class MainDbContext : DbContext
    {
        private readonly string connectionString;

        public MainDbContext(string connectionString)
        {
            this.connectionString = connectionString;
            Database.EnsureCreated();
        }

        public DbSet<User> users { get; set; }
        public DbSet<State> states { get; set; }
        public DbSet<Subscription> subscriptions { get; set; }
        public DbSet<Ticket> tickets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Console.Out.WriteLine(connectionString);
            optionsBuilder.UseNpgsql(connectionString);
            // optionsBuilder.LogTo(System.Console.WriteLine);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(user => user.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<User>().Property(user => user.ChatId).IsRequired();
            modelBuilder.Entity<User>().HasIndex(u => u.ChatId).IsUnique();
            modelBuilder.Entity<State>().Property(state => state.Data).HasColumnType("jsonb");
            modelBuilder.Entity<Subscription>().Property(subscription => subscription.Data).HasColumnType("jsonb");
            modelBuilder.Entity<Ticket>().HasIndex(ticket => ticket.Id).IsUnique();
        }
    }
}