using System;

namespace TraderBot.entities
{
    public static class MathExtensions
    {
        public static Candle Min(Candle a, Candle b)
        {
            return a.Low <= b.Low ? a : b;
        }

        public static Candle Max(Candle a, Candle b)
        {
            return a.High >= b.High ? a : b;
        }
    }

    public class Candle
    {
        public Candle()
        {
        }

        public Candle(DateTime begin, DateTime end, double open = 0, double close = 0, double low = 0, double high = 0,
            double value = 0, double volume = 0)
        {
            Begin = begin;
            End = end;
            Open = open;
            Close = close;
            Low = low;
            High = high;
            Value = value;
            Volume = volume;
        }

        public string Ticket { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Value { get; set; }
        public double Volume { get; set; }
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }

        public double DurationInMinutes => Convert.ToInt32(End.Subtract(Begin).TotalMinutes);

        public int CompareTo(Candle other)
        {
            if (Close < other.Close) return -1;
            if (Math.Abs(Close - other.Close) < 1e-9) return 0;

            return 1;
        }

        public int CompareTo(object obj)
        {
            if (obj is Candle candle) return CompareTo(candle);
            throw new ArgumentException("huj");
        }

        public bool Equals(Candle other)
        {
            return other.Begin == Begin;
        }

        public override bool Equals(object obj)
        {
            if (obj is Candle candle) return Equals(candle);
            return false;
        }

        public override string ToString()
        {
            return
                $"start:{Begin} stop:{End} open:{Open} close:{Close} low:{Low} high:{High} value:{Value} volume:{Volume}";
        }
    }
}