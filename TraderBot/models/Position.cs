namespace TraderBot.models
{
    public enum Position
    {
        Start,
        TicketInfo,
        TicketInfoWaiting,
        SubscriptionSettingTicketWaiting,
        SubscriptionSettingType,
        SubscriptionSettingParamsWaiting,
        SubscriptionCreated
    }
}