namespace TraderBot.models
{
    public class ChatData
    {
        public string Ticket { get; set; }

        public string SubscriptionType { get; set; }
    }
}