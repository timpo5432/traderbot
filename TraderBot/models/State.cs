namespace TraderBot.models
{
    public class State
    {
        public int Id { get; set; }
        public string CurrentPosition { get; set; }

        public string Data { get; set; }
    }
}