using System.Collections.Generic;

namespace TraderBot.models
{
    public class User
    {
        public int Id { get; set; }
        public long ChatId { get; set; }
        public string NickName { get; set; }

        public State State { get; set; }

        public List<Subscription> Subscriptions { get; set; }
    }
}