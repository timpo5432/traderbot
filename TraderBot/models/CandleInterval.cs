namespace TraderBot.entities
{
    public enum CandleInterval
    {
        One = 1,
        Five = 5,
        Ten = 10
    }
}