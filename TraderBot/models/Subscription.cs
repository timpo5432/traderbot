namespace TraderBot.models
{
    public class Subscription
    {
        public int Id { get; set; }

        public string Type { get; set; }

        //Active, Paused...
        public int Status { get; set; }

        public string CronId { get; set; }
        public string Data { get; set; }
    }
}