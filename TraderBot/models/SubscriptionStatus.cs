namespace TraderBot.models
{
    public enum SubscriptionStatus
    {
        Active,
        Paused,
        Draft
    }
}