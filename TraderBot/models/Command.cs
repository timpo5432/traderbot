namespace TraderBot.models
{
    public enum Command
    {
        Start,
        Help,
        TicketInfo,
        TicketInfoWaiting,
        ListSubscriptions,
        AddSubscription,
        AddSubscriptionComplete,
        SubscriptionType,
        SubscriptionTypeWaiting,
        Predict,
        SwitchSubscription,
        DeleteSubscription
    }
}