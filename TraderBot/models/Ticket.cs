namespace TraderBot.models
{
    public class Ticket
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Source { get; set; }

        public string LastCandleEndDateTime { get; set; }
    }
}