﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Ninject;
using Ninject.Extensions.Conventions;
using Quartz.Spi;
using Telegram.Bot;
using TraderBot.entities;
using TraderBot.models;
using TraderBot.services;

namespace TraderBot
{
    internal class Program
    {
        public static MyApp Configure()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json").Build();

            var container = new StandardKernel();

            container.Bind<IConfigurationRoot>().ToConstant(config);
            container.Bind<MyApp>().To<MyApp>();
            container.Bind<UnionActionPriceService>().To<UnionActionPriceService>();
            
            container.Bind<EFGenericRepository<User>>().To<EFGenericRepository<User>>();
            container.Bind<EFGenericRepository<Subscription>>().To<EFGenericRepository<Subscription>>();
            container.Bind<EFGenericRepository<State>>().To<EFGenericRepository<State>>();
            container.Bind<EFGenericRepository<Ticket>>().To<EFGenericRepository<Ticket>>();
            
            container.Bind<UpdateTicketListJob>().To<UpdateTicketListJob>();
            container.Bind<IJobFactory>().To<NinjectJobFactory>();
            
            container.Bind<ISubscription>().To<PeriodicSubscription>();
            container.Bind<ISubscription>().To<ReachValueSubscription>();
            container.Bind<ISubscription>().To<ChangePercentsSubscription>();

            container.Bind<LogicService>().To<LogicService>().WithConstructorArgument("connection", config.GetConnectionString("Postgres"));
            container.Bind<Handlers>().To<Handlers>();

            container.Bind(x =>
                x.FromThisAssembly()
                    .SelectAllClasses()
                    .Where(t => t.GetInterfaces().Any(i => i == typeof(IActionPriceService)))
                    .BindAllInterfaces()
            );

            var bot = new TelegramBotClient(config.GetConnectionString("TelegramToken"));


            container.Bind<ITelegramBotClient>().ToConstant(bot);
            
            container.Bind<IPredictor>().To<TechnicalAnalysisPredictor>();
            container.Bind<IPredictor>().To<MachineLearningPredictor>()
                .WithConstructorArgument(typeof(string), config.GetConnectionString("Api"));

            container.Bind<DbContext>().To<MainDbContext>()
                .InTransientScope()
                .WithConstructorArgument(typeof(string), config.GetConnectionString("Postgres"));

            container.Bind<IActionsDataRepository>().To<InfluxDbActionsRepository>()
                .WithConstructorArgument("connectionString", config.GetConnectionString("Influx"));
            return container.Get<MyApp>();
        }

        private static void Main(string[] args)
        {
            Configure().Start().GetAwaiter().GetResult();
        }
    }
}